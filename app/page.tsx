import Image from "next/image";
import Link from "next/link";

export default function Home() {
  return (
    <main className="flex min-h-full flex-col items-center justify-between p-24">
      <div>Hello world</div>
      <Link href="/blog">Blog</Link>
      <Link href="/products">Products</Link>
    </main>
  );
}
