import Card from "@/components/card";
import Link from "next/link";

export default function ArchivedNotifications() {
  return (
    <Card>
      <div>archived Notifications</div>
      <Link href="/complex-dashboard">Default</Link>
    </Card>
  );
}
